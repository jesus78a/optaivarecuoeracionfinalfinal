package com.optativa.optativacomputadoras;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.optativa.optativacomputadoras.BaseDeDatosSqlite.Computador;
import com.optativa.optativacomputadoras.BaseDeDatosSqlite.OpenHelper;

import java.util.ArrayList;

public class MostrarActivity extends AppCompatActivity {

    ListView listViewComputador;
    ArrayList<Computador> listaComputador;
    ArrayList<String> listaInformacion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar);

        listViewComputador = findViewById(R.id.listViewComputadoras);

        consultarComputador();

        ArrayAdapter adaptador = new ArrayAdapter(this,
                R.layout.support_simple_spinner_dropdown_item, listaInformacion);

        listViewComputador.setAdapter(adaptador);

        listViewComputador.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final CharSequence[] opciones={"Modificar","Eliminar","Cancelar"};
                AlertDialog.Builder alertOpciones = new AlertDialog.Builder(MostrarActivity.this);
                alertOpciones.setTitle("Seleccione una opcion");
                alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        if (opciones[i].equals("Modificar")){
                            Intent intent = new Intent(MostrarActivity.this, ModificarActivity.class);
                            intent.putExtra("id", listaComputador.get(position).getId());
                            intent.putExtra("nserie", listaComputador.get(position).getNserie());
                            intent.putExtra("descripcion", listaComputador.get(position).getDescripcion());
                            intent.putExtra("procesador", listaComputador.get(position).getProcesador());
                            intent.putExtra("conectividad", listaComputador.get(position).getConectividad());
                            intent.putExtra("memoria", listaComputador.get(position).getMemoria());
                            intent.putExtra("discoduro", listaComputador.get(position).getDiscoduro());
                            intent.putExtra("sistemaop", listaComputador.get(position).getSistemaop());
                            intent.putExtra("puertos", listaComputador.get(position).getPuertos());
                            startActivity(intent);
                            finish();
                        }else if (opciones[i].equals("Eliminar")){
                            OpenHelper conexion = new OpenHelper(MostrarActivity.this, "Computador", null, 1);
                            SQLiteDatabase db = conexion.getWritableDatabase();
                            db.delete("Computador","id="+ listaComputador.get(position).getId(),null);
                            db.close();
                            Toast.makeText(MostrarActivity.this, "Se eliminó cexitosamente", Toast.LENGTH_SHORT).show();
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        }else {
                            dialog.dismiss();
                        }
                    }
                });alertOpciones.show();
            }
        });



    }

    private void consultarComputador() {
        OpenHelper conexion = new OpenHelper(this, "Computador", null, 1);
        SQLiteDatabase db = conexion.getReadableDatabase();
        listaComputador = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from Computador", null);
        while (cursor.moveToNext()){
            Computador computador = new Computador();
            computador.setId(cursor.getInt(0));
            computador.setNserie(cursor.getString(1));
            computador.setDescripcion(cursor.getString(2));
            computador.setProcesador(cursor.getString(3));
            computador.setConectividad(cursor.getInt(4));
            computador.setMemoria(cursor.getString(5));
            computador.setDiscoduro(cursor.getString(6));
            computador.setSistemaop(cursor.getString(7));
            computador.setPuertos(cursor.getString(8));
            listaComputador.add(computador);
        }
        mostrarLista();
    }

    private void mostrarLista() {
        listaInformacion = new ArrayList<>();
        for (int i=0; i<listaComputador.size(); i++){
            listaInformacion.add("NSERIE: " + listaComputador.get(i).getNserie()  + " DESCRIPCION: " + listaComputador.get(i).getDescripcion());
        }

    }
}